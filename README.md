# Image_registration_DJI_Mavic_3M

The DJI Mavic 3M can take multispectral images, but the TIF images of the different bands are not taken at exactly the same time.  
This results in a small misalignment of the images. When these images are then processed with Open Drone Map (https://www.opendronemap.org/) to create an orthophoto there is often a misalignment of the bands. 
The developers are aware of this issue and it might be fixed at this point.  
This script is a pre-processing of the images to align each set of 4 images before using them in ODM.  
When using the images in ODM make sure to turn off the band alignment.  
  
The script can be run from a shell by adding the directory with the images as input parameter, as well as the number of threads that shall be used for the computation (depends on your computer).  
Example:    
`python "C:\Users\yario\Code\Multi_spectral_image_coregistration\coregistration_mavic_3m_images.py" "C:\\Users\\yario\\data\\test_data\\" 10`  
  
Before image registration:  
![before](before.png)

After image registration:  
![after](after.png)